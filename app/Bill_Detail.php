<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill_Detail extends Model
{
    protected $table = 'bill_details';

    protected $fillable = ['bill_id','product_id','quantity','price'];
}
