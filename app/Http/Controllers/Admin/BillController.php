<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Customer;
use App\Bill;
use App\Bill_Detail;
use DB;
use Request;
use Session;

class BillController extends Controller
{
    public function index(){
        $bills = DB::table('bills')
                            ->join('customers','customers.id','=','bills.customer_id')
                            ->select('customers.*','bills.*','customers.name as customer_name','bills.created_at as bill_date','customers.id as customer_id')
                            ->orderBy('bills.id','DESC')
                            ->get();
    	// $bills = Bill::orderBy()->get();

    	return view('admin.particles.bill',compact('bills'));
    }

    public function edit($id){

    	$bill = Bill::find($id);

        $customer = DB::table('customers')
                            ->join('bills','bills.customer_id','=','customers.id')
                            ->select('customers.*','bills.*','bills.created_at as date_order','bills.note as bill_note')
                            ->where('bills.id','=',$bill->id)
                            ->first();

    	$bill_details = DB::table('bill_details')
    						->join('products', 'products.id','=','bill_details.product_id')
    						->select('bill_details.*', 'products.*','products.name as product_name','bill_details.quantity as bill_detail_quantity')
    						->where('bill_details.bill_id', $bill->id)
    						->get();

        $status = ['Đang chờ xác nhận','Đã xác nhận','Đang vận chuyển','Đã giao hàng','Đã hủy'];

    	return view('admin.particles.billl-detail', compact('customer','bill','bill_details','status'));
    }

    public function update($id){
        $bill = Bill::find($id);
        $bill->status = Request::get('status');
        $bill->save();

        return redirect()->route('bills.index');
    }

    public function destroy($id){
        $bill = Bill::find($id);
        $bill->delete();

        Session::flash('message', "Đã xóa đơn hàng");

        return redirect()->back();
    }
}
