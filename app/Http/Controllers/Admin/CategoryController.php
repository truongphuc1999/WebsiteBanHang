<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use DB;
use Session;
// use App\Http\Controller\CategoryController;

class CategoryController extends Controller
{
	public function index(){
		$this->data['title'] = 'List category';
        $listCate = DB::table('categories')
            ->orderBy('id', 'desc')
            ->paginate(10);
        $this->data['listCate'] = $listCate;

        return view('admin.particles.category', $this->data);
	}

    public function create(){
        $this->data['title'] = "Add Category";
        $listCate = DB::table('categories')
            ->orderBy('id','desc')->get();
        $this->data['listCate'] = $listCate;

    	return view('admin.particles.category-create', $this->data);
    }

    public function store(Request $request){
    	$category = new Category;
    	$category->name = $request->name;
    	$category->slug = $request->slug;
    	// $category->desc = $request->txtDesc;
    	$category->parent_id = $request->has('parent_id') ? $request->parent_id : 0;
    	$category->meta_title = $request->meta_title;
    	$category->meta_description = $request->meta_description;

    	$category->save();

    	Session::flash('message', "Đã thêm danh mục");

    	return redirect()->back();
    }

    public function show($id){

    }

    public function edit($id){
        $category = Category::find($id);
        $this->data['title'] = 'Edit Category';
        $this->data['category'] = $category;
        $this->data['listCate'] = DB::table('categories')->get();

        return view('admin.particles.category-create', $this->data); 
    }

    public function update(Request $request, $id){
        $category = Category::find($id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->parent_id = $request->has('parent_id') ? $request->parent_id : 0;
        $category->meta_title = $request->meta_title;
        $category->meta_description = $request->meta_description;

        $category->save();

        Session::flash('message', "Đã sửa danh mục");

        return redirect('/admin/categories');
    }

    public function destroy($id){
        // Lấy số lượng category có danh mục cha là danh mục muốn xóa
        $parent_count = Category::where('parent_id', $id)->count();
        
        // Nếu số lượng = 0 thì cho phép xóa
        if($parent_count == 0){
            $category = Category::find($id);
            $category->delete();

            //Nếu sản phẩm nào thuộc danh mục đã xóa chuyển sang danh mục chưa xác định
            $products = Product::where('category_id',$id)->get();
            if(count($products) > 0){
                foreach ($products as $key => $product) {
                    $product->category_id = 0;
                    $product->save();
                }
            }

            Session::flash('message', "Đã xóa danh mục");

            return redirect()->back();
        }else{
            Session::flash('message', "Không thể xóa danh mục này");

            return redirect()->back();
        }
    }
}
