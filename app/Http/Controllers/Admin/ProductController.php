<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Product_Image;
use App\Category;
use Image;
use DB;
use Session;
use File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'List product';

        $listProduct = DB::table('products')
            ->orderBy('id','desc')
            ->paginate(10);
        $this->data['listProduct'] = $listProduct;

        return view('admin.particles.product', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = "Add product";
        $listCate = DB::table('categories')
            ->orderBy('id','desc')->get();
        $this->data['listCate'] = $listCate;

        return view('admin.particles.product-create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Tạo mới một sản phẩm và gán giá trị cho các trường 
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->percent_sale = $request->percent_sale;
        $product->quantity = $request->quantity;
        $product->slug = $request->slug;
        $product->category_id = $request->category_id;
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        //Nếu có upload ảnh thì
        if($request->hasFile('upload_images')){

            //Lấy tất cả các file đã chọn gán vào biến $images
            $images = $request->file('upload_images');
            //Khởi tạo biến $temp = 0 dùng để gán giá trị tạm thời cho $product_id của ảnh
            $temp = 0;

            foreach ($images as $key => $image) {
                //Khởi tạo tên cho ảnh
                $filename = $product->slug . '-' . ($key + 1) . '.' . $image->getClientOriginalExtension();
                //Khởi tạo vị trí lưu ảnh
                $location = public_path('\images\content\products' . '\\' . $filename);

                //Thay đổi kích thước -> lưu vào vị trí đã khởi tạo
                // Image::make($image)->resize(270,270)->save($location);
                Image::make($image)->save($location);

                //Tạo mới một ảnh của sản phẩm và gán dữ liệu cho các trường
                $product_image = new Product_Image;
                $product_image->name = $filename;
                $product_image->product_id = $temp;
                $product_image->save();
            }

            //Gán tên của ảnh mặc định là ảnh thứ nhất
            $product->image = Product_Image::where('product_id',$temp)->first()->name;
        }

        $product->save();

        //Cập nhật $product_id của Product_Image đúng với Product đang thêm mới
        $product_image->where('product_id', $temp)->update(['product_id' => $product->id]);

        Session::flash('message', "Đã thêm sản phẩm");

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        $product_images = Product_Image::where('product_id',$product->id)->get();

        $categories = Category::get();

        return view('admin.particles.product-create',compact('product','product_images','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->percent_sale = $request->percent_sale;
        $product->quantity = $request->quantity;
        $product->slug = $request->slug;
        $product->category_id = $request->category_id;
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        $images = Product_Image::where('product_id', $id)->get();

        //Xóa ảnh
        foreach ($images as $key => $image) {
            if(!in_array($image->name, $request->image_current)){
                $file = '\images\content\products'. '\\' . $image->name;

                //Nếu file này có tồn tại
                if(File::exists($file))
                    File::delete($file);

                $product_image = Product_Image::find($image->id);
                $product_image->delete();
            }
        }

        //Thêm ảnh mới
        if($request->hasFile('upload_images')){

            //Lấy tất cả các file đã chọn gán vào biến $images
            $upload_images = $request->file('upload_images');

            foreach ($upload_images as $key => $image) {
                $max_product_image = Product_Image::where('product_id',$id)->orderBy('id','DESC')->first();

                // return dd($max_product_image);

                //Khởi tạo tên cho ảnh
                $filename = $product->slug . '-' . ($max_product_image->id + 1) . '.' . $image->getClientOriginalExtension();

                //Khởi tạo vị trí lưu ảnh
                $location = public_path('\images\content\products' . '\\' . $filename);

                //Thay đổi kích thước -> lưu vào vị trí đã khởi tạo
                // Image::make($image)->resize(270,270)->save($location);
                Image::make($image)->save($location);

                //Tạo mới một ảnh của sản phẩm và gán dữ liệu cho các trường
                $product_image = new Product_Image;
                $product_image->name = $filename;
                $product_image->product_id = $id;
                $product_image->save();
            }

            //Gán tên của ảnh mặc định là ảnh thứ nhất
            $product->image = Product_Image::where('product_id',$id)->first()->name;
        }
        $product->save();

        Session::flash('message', "Đã cập nhật dữ liệu.");

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        Session::flash('message', "Đã xóa sản phẩm");

        return redirect()->back();
    }
}
