<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tranport_Province;
use App\Tranport_District;
use App\Province;
use App\District;
use DB;

class TranportController extends Controller
{
    public function index()
    {
        $tranport_province = Tranport_Province::all();
        $province = Province::orderBy('name')->get();

        return view('admin.particles.config-tranport',compact('tranport_province','province'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $tranport_province = new Tranport_Province;
        $tranport_province->name = 'Giao hàng tận nơi';
        $tranport_province->province_id = $request->province;
        $tranport_province->price_from = 0;
        $tranport_province->price_to = 0;
        $tranport_province->price_shipping = $request->price;
        $tranport_province->denied = 0;
        $tranport_province->save();

        if($request->province != 0){
            $districts = District::where('province_id',$request->province)->get();

            foreach ($districts as $district){
                Tranport_District::insert([
                    'province_tranport_id' => $tranport_province->id,
                    'district_id' => $district->id,
                    'price_from' => 0,
                    'price_to' => 0,
                    'price_shipping' => $request->price,
                    'denied' => 0
                ]);
            }
        }

        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tranport_province = Tranport_Province::find($id);
        $tranport_district = DB::table('tranports_district')
                                ->join('districts','tranports_district.district_id','=','districts.id')
                                ->where('tranports_district.province_tranport_id', $tranport_province->id)
                                ->select('tranports_district.*', 'districts.name as district_name')->get();

        return response()->json(['tranport_province' => $tranport_province, 'tranport_district' => $tranport_district]);
    }

    public function update(Request $request, $id)
    {
        $tranport_province = Tranport_Province::find($id);
        $tranport_province->name = $request->name_edit;
        $tranport_province->price_shipping = $request->price_edit;
        $tranport_province->save();

        $tranport_district = Tranport_District::where('province_tranport_id', $id)->get();

        foreach ($tranport_district as $key => $item) {
            $temp = 'price_edit_' . ($key + 1);
            $item->price_shipping = $tranport_province->price_shipping + $request->$temp;
            $item->save();
        }
    }

    public function destroy($id)
    {
        $tranport_district = Tranport_District::where('province_tranport_id', $id)->delete();

        $tranport_province = Tranport_Province::find($id);
        $tranport_province->delete();
    }
}
