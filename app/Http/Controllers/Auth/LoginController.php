<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Http\Requests\LoginRequest;

use Auth;
use middleware;

use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //Đăng nhập
    public function getLogin()
    {
        return view('auth.login');
    }

    //Kiểm tra đăng nhập
    public function postLogin(LoginRequest $request)
    {
        $user = [
            'email' => $request->txtEmail,
            'password' => $request->txtPassword
        ];

        if(Auth::attempt($user)){
            return redirect('admin');
        }
        else{
            Session::flash('message', "Email hoặc mật khẩu không đúng");
            return redirect()->back();
        }
    }

    //Đăng xuất
    public function getLogout()
    {
        Auth::logout();

        return redirect()->route('getLogin');
    }
}
