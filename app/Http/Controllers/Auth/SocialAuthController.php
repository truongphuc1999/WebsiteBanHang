<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Services\SocialAccountService;
use Illuminate\Support\Facades\Hash;
use Socialite;
use Auth;
use App\SocialAccount;
use App\User;

class SocialAuthController extends Controller
{
    public function redirectToProvider($social)
    {
        return Socialite::driver($social)->redirect();
    }

     public function handleProviderCallback($social)
    {
        $socialUser = Socialite::driver($social)->stateless()->user();

        $socialProvider = SocialAccount::where('provider_user_id', $socialUser->getId())->first();

        if(!$socialProvider){
        	$user = new User;
        	$user->name = $socialUser->getName();
        	$user->email = $socialUser->getEmail();
        	$user->password = Hash::make('123456');
        	$user->role_key = 0;
        	$user->save();

        	$socialAccount = new SocialAccount;
        	$socialAccount->user_id = $user->id;
        	$socialAccount->provider_user_id = $socialUser->getId();
        	$socialAccount->provider = $social;
        	$socialAccount->save();
        }else{
            $user = User::find($socialProvider->user_id);
        }

        Auth::login($user);

        return redirect()->route('home');
    }
}
