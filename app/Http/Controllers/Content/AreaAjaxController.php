<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Request;
use App\District;
use App\Ward;

class AreaAjaxController extends Controller
{
    public function loadDistricts(){
        $data = '';

        if(Request::get('province_id') != ""){
            $districts = District::where('province_id',Request::get('province_id'))->orderBy('name')->get();

            $data = '<option value="">— Chọn Quận/Huyện —</option>';
            foreach ($districts as $district) {
                $data.= '<option value="' . $district->id . '">' . $district->name . '</option>';
            }

        }else{
            $data = '<option value="">— Chọn Quận/Huyện —</option>';
        }

        return response()->json(['districts' => $data]);
    }

    public function loadWards(){
        $data = '';

        if(Request::has('district_id')){
            $wards = Ward::where('district_id',Request::get('district_id'))->orderBy('name')->get();

            foreach ($wards as $ward) {
                $data.= '<option value="' . $ward->id . '">' . $ward->name . '</option>';
            }
        }else{
            $data = '<option value="">— Chọn Phường/Xã —</option>';
        }

        return response()->json(['wards' => $data]);
    }
}
