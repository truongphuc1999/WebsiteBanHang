<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Cart;
use App\Product;
use DB;
use Request;
use Session;

class CartController extends Controller
{
	//Hiển thị danh sách sản phẩm trong giỏ hàng
    public function index(){
        
        $cart = Cart::content();

        //Cập nhật lại thông tin sản phẩm cho giỏ hàng
        foreach ($cart as $item) {
            $product = Product::find($item->id);

            //Nếu sản phẩm đã bị xóa thì ...
            if($product == null){
                //Xóa luôn sản phẩm đó trong giỏ hàng
                Cart::remove($item->rowId);
            }
            else{
                //Nếu số lượng sản phẩm trong kho nhỏ hơn số lượng sản phẩm trong giỏ hàng thì ...
                if($product->quantity < $item->qty){
                    //Cập nhật lại thông tin sản phẩm trong giỏ hàng (với số lượng là số lượng còn tồn trong kho)
                    Cart::update($item->rowId, ['name' => $product->name, 'price' => $product->price - ($product->price / 100 * $product->percent_sale), 'qty'=> $product->quantity, 'option' => ['img' => $product->image, 'slug' => $product->slug]]);
                }else{
                    //Cập nhật lại thông tin sản phẩm trong giỏ hàng (trừ số lượng ra)
                    Cart::update($item->rowId, ['name' => $product->name, 'price' => $product->price - ($product->price / 100 * $product->percent_sale), 'option' => ['img' => $product->image, 'slug' => $product->slug]]);
                }
            }
        }

    	//Tăng số lượng sản phẩm trong giỏ hàng
    	if (Request::get('product_id') && (Request::get('increment')) == 1) {
            $rows = Cart::search(function($key, $value) {
                return $key->id == Request::get('product_id');
            });
            $item = $rows->first();

            $product = Product::find(Request::get('product_id'));
            if($item->qty < $product->quantity){
                Cart::update($item->rowId, $item->qty + 1);
            }else{
                Session::flash('popup', "Số lượng không đủ cung ứng");
                return redirect()->back();     
            }
        }

        //Giảm số lượng sản phẩm trong giỏ hàng
        if (Request::get('product_id') && (Request::get('decrease')) == 1) {
            $rows = Cart::search(function($key, $value) {
                return $key->id == Request::get('product_id');
            });
            $item = $rows->first();
            Cart::update($item->rowId, $item->qty - 1);
        }
        
        //Lấy số lượng sản phẩm trong giỏ hàng
    	$count = Cart::count();
        //Lấy tổng tiền của giỏ hàng như không tính thuế
    	$totalPrice = Cart::subtotal(0,',','.');

    	return view('content.cart',compact('cart', 'count', 'totalPrice'));
    }

    //Thêm một sản phẩm vào giỏ hàng
    public function add(){
    	$product = Product::find(Request::get('product_id'));

        //Lấy sản phẩm ở trong giỏ hàng (dựa vào product_id)
        $rows = Cart::search(function($key, $value) {
            return $key->id == Request::get('product_id');
        });

        if(count($rows) > 0){
            //Biến dùng để chứa số lượng sản phẩm (đang thêm vào) ở trong giỏ hàng
            $qtyCart = ($rows->first())->qty;

            //Kiểm tra số lượng tồn kho
            if($product->quantity < Request::get('quantity') + $qtyCart){
                Session::flash('message', "Số lượng không đủ cung ứng");
                return redirect()->back();
            }
        }else{
            //Kiểm tra số lượng tồn kho
            if($product->quantity < Request::get('quantity')){
                Session::flash('message', "Số lượng không đủ cung ứng");
                return redirect()->back();
            }    
        }

        Cart::add(array('id' => $product->id, 'name' => $product->name, 'qty' => Request::get('quantity'), 'price' => $product->price - ($product->price / 100 * $product->percent_sale), 'options' => array('img' => $product->image, 'slug' => $product->slug )));

        return redirect()->route('cart');
    }

    //Xóa sản phẩm trong giỏ hàng
    public function delete(){
    	Cart::remove(Request::get('rowId'));

        return response()->json(['count' => Cart::count(), 'total' => Cart::subtotal()]);
    }

    //Cập nhật sản phẩm trong giỏ hàng
    public function update(){
        $rows = Cart::search(function($key, $value) {
            return $key->rowId == Request::get('rowId');
        });

        $item = $rows->first();

        $product = Product::find($item->id);

        if(Request::get('quantity') <= $product->quantity){
            Cart::update(Request::get('rowId'),Request::get('quantity'));
            return response()->json(['count' => Cart::count(), 'total' => Cart::subtotal(),'countQty' => $item->qty]);
        }else{
            Session::flash('popup', $product->name." chỉ còn ".$product->quantity." sản phẩm.");
            return response()->json(['count' => Cart::count(),'countQty' => $item->qty]);     
        }
    }

    //Xóa toàn bộ sản phẩm trong giỏ hàng
    public function destroy(){
    	Cart::destroy();

    	return response()->json(['count' => Cart::count(), 'total' => Cart::subtotal()]);
    }
}
