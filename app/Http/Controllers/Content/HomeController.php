<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')->select('name','price','percent_sale','image','description','slug')->orderBy('id','DESC')->get();
        $categories = DB::table('categories')->select('id','name')->where('parent_id',0)->get();

        return view('content.home',compact('products','categories'));
    }
}
