<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cart;
use App\Customer;
use App\Product;
use App\Bill;
use App\Bill_Detail;
use Carbon;
use App\Province;
use App\District;
use App\Ward;
use App\Tranport_Province;
use App\Tranport_District;
use Auth;
use Session;

class OrderController extends Controller
{
    public function index(){
        //Lấy ra giỏ hàng
        $cart = Cart::content();

        $customers = "";

        $provinces = Province::orderBy('name')->get();

        if(Auth::check()){
            $customers = Customer::where('user_id',Auth::user()->id)->where('status',1)->first();

            if($customers != null){
                $districts = District::where('province_id', $customers->province)->orderBy('name')->get();
                $wards = Ward::where('district_id', $customers->district)->orderBy('name')->get();
                $price_shipping = Tranport_District::where('district_id', $customers->district)->get()[0]->price_shipping;
            }

            return view('content.order', compact('cart','provinces','districts','wards','price_shipping','customers'));
        }

    	if(Cart::count() > 0){
    		return view('content.order', compact('cart','provinces','customers'));
    	}
    }

    public function store(Request $request){
        if(Auth::check()){
            $customer = Customer::where('user_id', Auth::user()->id)->where('status', 1)->get();
            if($request->temp == 2 || count($customer) == 0){
                $customer = new Customer;

                $customer->name = $request->name;
                $customer->email = $request->email;
                $customer->phone = $request->phone;
                $customer->note = "";
                $customer->address = $request->address;
                $customer->province = $request->province;
                $customer->district = $request->district;
                $customer->ward = $request->ward;
                $customer->user_id = Auth::user()->id;
                $customer->status = count(Customer::where('user_id', Auth::user()->id)->get()) > 0 ? 0 : 1;

                $customer->save();
            }
        }else{
            $customer = new Customer;

            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->phone = $request->phone;
            $customer->note = "";
            $customer->address = $request->address;
            $customer->province = $request->province;
            $customer->district = $request->district;
            $customer->ward = $request->ward;
            $customer->user_id = 0;
            $customer->status = 0;

            $customer->save();
        }

    	$cartContent = Cart::content();

    	$bill = new Bill;

    	$bill->customer_id = $customer->id;
    	$bill->date_order = Carbon\Carbon::now();
    	$bill->totalPrice = str_replace(',', '', Cart::subtotal(0,0,',','.'));

        $tranport_district = Tranport_District::where('district_id', $customer->district)->get();

        if(count($tranport_district) > 0){
            $bill->price_shipping = $tranport_district[0]->price_shipping;
        }else{
            $tranport_province = Tranport_Province::where('province_id', 0)->get();

            if(count($tranport_province) > 0){
                $bill->price_shipping = $tranport_province[0]->price_shipping;
            }else{
                $customer->delete();
                Session::flash('message', "Khu vực này không hỗ trợ vận chuyển");

                return redirect()->back();
            }
        }

        $bill->status = 0;
    	$bill->note = $request->note == null ? "" : $request->note;

    	$bill->save();

    	if(count($cartContent) > 0){
    		foreach ($cartContent as $item) {
                $product = Product::find($item->id);

    			$bill_detail = new Bill_Detail;

    			$bill_detail->bill_id = $bill->id;
    			$bill_detail->product_id = $item->id;
                $bill_detail->product_name = $product->name;
    			$bill_detail->quantity = $item->qty;
    			$bill_detail->price = $item->price;
                $bill_detail->status = 0;
                $bill_detail->note = "";

    			$bill_detail->save();

                //Cập nhật lai số lượng sản phẩm
                $product->quantity = $product->quantity - $bill_detail->quantity;

                $product->save();
    		}
    	}

    	Cart::destroy();

    	return view('content.particles.order-success',compact('bill'));
    }

    public function loadPriceShipping(Request $request){
        $tranport_district = Tranport_District::where('district_id', $request->district_id)->select('price_shipping')->get();


        if (count($tranport_district) == 0) {
            $temp = Tranport_Province::where('province_id', 0)->get();

            if(count($temp) > 0){
                $price_shipping = number_format($temp[0]->price_shipping,0,',','.');

                $total_price = number_format(Cart::subtotal(0,',','') + number_format($temp[0]->price_shipping,0,',',''), 0, ',','.');
            }else{  
                $price_shipping = "Khu vực không hỗ trợ vận chuyển";
                $total_price = Cart::subtotal(0,',','.');
            }
        }else{
            $price_shipping = number_format($tranport_district[0]->price_shipping,0,',','.');
            $total_price = number_format(Cart::subtotal(0,',','') + number_format($tranport_district[0]->price_shipping, 0, ',', ''), 0, ',', '.');
        }

        return response()->json(['price_shipping' => $price_shipping, 'total_price' => $total_price]);
    }
}
