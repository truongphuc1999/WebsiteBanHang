<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Product_Image;

class ProductController extends Controller
{
    public function showProductDetail($product_slug){
    	$product = Product::where('slug', $product_slug)->first();
    	$images_detail = Product_Image::where('product_id',$product->id)->get();

    	// return dd($images_detail);

    	return view('content.product-details', compact('product','images_detail'));
    }
}
