<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Nếu đã đăng nhập thì ...
        if(Auth::check())
        {
            $user = Auth::user();
            //Nếu đăng nhập không phải là Khách hàng thì ...
            if($user->role_key != 0) // Trong bảng Role giá trị key = 0 là Khách hàng
                return $next($request);
            else
                return redirect('/home');
        }
        else{
            return redirect('/login');
        }
    }
}
