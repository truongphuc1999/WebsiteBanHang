<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name','price','percent_sale','description','slug','category_id','meta_title','meta_description'];

    public function product_image(){
    	return $this->hasMany();
    }
}
