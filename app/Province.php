<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
   	protected $fillable = ['name', 'type', 'code'];

   	public function tranport_province(){
   		return $this->hasOne('App\Tranport_Province');
   	}
}
