<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tranport_District extends Model
{
    protected $table = 'tranports_district';
    protected $fillable = ['province_tranport_id', 'district_id', 'price'];
}
