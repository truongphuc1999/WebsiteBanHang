<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tranport_Province extends Model
{
    protected $table = 'tranports_province';
    protected $fillable = ['name','province_id', 'price'];

    public function province(){
    	return $this->belongsTo('App\Province');
    }
}
