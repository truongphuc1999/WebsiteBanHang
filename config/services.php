<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '239377926811901',
        'client_secret' => '82ca98bf275acbdf1dbaf62d0765567d',
        'redirect' => 'http://localhost/login/facebook/callback',
    ],

    'google' => [
        'client_id' => '215543688799-dd51ifv92et6kdb0hve1e0ocbcfi5jc6.apps.googleusercontent.com',
        'client_secret' => '1LicDeFfQL67fddjM9F-oUwG',
        'redirect' => '/login/google/callback',
    ],
];
