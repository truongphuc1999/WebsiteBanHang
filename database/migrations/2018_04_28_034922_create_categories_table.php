<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->comment('Đường dẫn rút gọn');
            $table->integer('parent_id')->nullable()->comment('Liên kết đến Category cha');
            $table->string('meta_title')->comment('Tiêu đề SEO');
            $table->string('meta_description')->comment('Nội dung SEO');
            $table->timestamps();
        });

        DB::table('categories')->insert([
            'name' => 'Chưa xác định',
            'slug' => 'chua-xac-dinh',
            'meta_title' => 'Chưa xác định',
            'meta_description' => 'Chưa xác định',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
