<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('price');
            $table->integer('percent_sale')->comment('Phần trăm giảm giá');
            $table->integer('quantity')->comment('Số lượng');
            $table->string('thumbnail')->comment('Ảnh hiển thị đại diện');
            $table->integer('category_id')->default(1)->comment('Liên kết đến Category');
            $table->text('description')->comment('Thông tin sản phẩm');
            $table->longText('content')->comment('Thông tin chi tiết sản phẩm');
            $table->string('slug')->comment('Đường dẫn rút gọn');
            $table->string('meta_title')->comment('Tiêu đề SEO');
            $table->string('meta_description')->comment('Nội dung SEO');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('SET DEFAULT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
