<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('extension')->comment('Phần đuôi mở rộng của file');
            $table->string('location')->comment('Vị trí lưu');
            $table->string('type')->comment('Kiểu file (image,video...)');
            $table->string('target')->comment('Nơi liên kết đến (sản phẩm, bài viết...)');
            $table->integer('target_id')->comment('Liên kết đến nơi liên kết');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
