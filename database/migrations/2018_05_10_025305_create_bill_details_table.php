<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_id')->comment('Liên kết đến Bill');
            $table->string('product_name');
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('quantity')->nullable();
            $table->double('price');
            $table->tinyInteger('status');
            $table->string('note');
            $table->timestamps();

            // $table->foreign('bill_id')->references('id')->on('bills')->onDelete('CASCADE');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_details');
    }
}
