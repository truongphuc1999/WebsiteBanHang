<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranportsDistrictTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tranports_district', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('province_tranport_id');
            $table->integer('district_id');
            $table->double('price_from');
            $table->double('price_to');
            $table->double('price_shipping');
            $table->boolean('denied');
            $table->foreign('province_tranport_id')->references('id')->on('tranports_province')->onDelete('CASCADE')
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tranports_district');
    }
}
