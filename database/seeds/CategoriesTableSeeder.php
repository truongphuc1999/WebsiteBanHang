<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	['name' => 'Thiết bị điện tử', 'slug' => 'thiet-bi-dien-tu', 'parent_id' => 0, 'meta_title' => 'Thiết bị điện tử', 'meta_description' => 'Thiết bị điện tử tốt nhất'],
        	['name' => 'Siêu thị tạp hóa', 'slug' => 'sieu-thi-tap-hoa', 'parent_id' => 0, 'meta_title' => 'Siêu thị tạp hóa', 'meta_description' => 'Siêu thị tạp hóa tốt nhất'],
        	['name' => 'Thời trang nam', 'slug' => 'thoi-trang-nam', 'parent_id' => 0, 'meta_title' => 'Thời trang nam', 'meta_description' => 'Thời trang nam đẹp nhất'],
        ]);
    }
}
