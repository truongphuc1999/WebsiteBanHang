<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	['name' => 'Apple iPhone X 64GB Space Grey', 'price' => '24990000', 'percent_sale' => 0, 'description' => '', 'slug' => 'apple-iphone-x-64gb-space-grey', 'category_id' => 3, 'meta_title' => 'Điện thoại Apple iPhone X 64GB Space Grey', 'meta_description' => 'Điện thoại iPhone X màu xám giá thấp nhất'],
        	['name' => 'Samsung Galaxy J7 2016 16GB (Vàng)', 'price' => '5690000', 'percent_sale' => 42, 'description' => '', 'slug' => 'samsung-galaxy-j7-2016-16gb-vang', 'category_id' => 3, 'meta_title' => 'Điện thoại Samsung Galaxy J7 2016 16GB (Vàng)', 'meta_description' => 'Điện thoại Samsung Galaxy J7 2016 giá tốt nhất'],
        	['name' => 'Nước ngọt có ga Coca-Cola vị cà phê lốc 6 chai 390ml', 'price' => '48000', 'percent_sale' => 0, 'description' => 'Sản phẩm là sự kết hợp sáng tạo giữa coca và cà phê quen thuộc tạo sản phẩm mới mang lại cảm giác sảng khoái, bừng tỉnh kèm theo vị đắng mùi thơm cà phê sữa độc đáo.', 'slug' => 'nuoc-ngot-co-ga-coca-cola-vi-ca-phe-loc-6-chai-390ml', 'category_id' => 4, 'meta_title' => 'Coca-Cola có ga vị cà phê', 'meta_description' => 'Coca-Cola hương vị mới nhất'],
        ]);
    }
}
