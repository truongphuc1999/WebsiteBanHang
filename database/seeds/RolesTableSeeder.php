<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        	['name' => 'Quản trị viên', 'key' => 1],
        	['name' => 'Khách hàng', 'key' => 0],
        ]);
    }
}
