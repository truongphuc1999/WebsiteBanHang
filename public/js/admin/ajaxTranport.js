// var save_province_tranport = function(event) {
// 	var province_id = $(event.target).parents(".modal-content").find("#province").val();
// 	var price_ship = $(event.target).parents(".modal-content").find("input[name='price']").val();

// 	// console.log(base_url + '/admin/tranports');

// 	$.ajaxSetup({
// 		headers: {
// 			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
// 		}
// 	});

// 	$.ajax({
// 		type : 'POST',
// 		url : base_url + '/admin/tranports',
// 		data : {'province_id' : province_id, 'price_ship' : price_ship},
// 		success : function(data){
// 			// console.log(data.districts);
// 			console.log('abc');
// 		}
// 	});
// }

var get_tranport_province = function(event) {
	var id_tranport = $(event.target).parents('.button').find('input').val();

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url : base_url + '/admin/tranports/' + id_tranport + '/edit',
		type : 'GET',
		data : '',
		success : function(data){
			var html = "";
			var htmlEdit = "";
			var url_route = base_url + '/admin/tranports/' + id_tranport;

			$(event.target).parents('section').find('#editPrice').remove();

			html = $(event.target).parents('section').find('#modalBodyEdit').html();

			htmlEdit += '<div id="editPrice">';
			$.each(data.tranport_district, function(key, item){
				htmlEdit += '<div class="form-group"><input type="checkbox" checked="true"> &nbsp;' + item['district_name'] + ': ' + item['price_shipping'] + ' đ ( ' + ( item['price_shipping'] - data.tranport_province['price_shipping']) + 'đ)<input type="number" name="price_edit_' + (key+1) + '" value="' + ( item['price_shipping'] - data.tranport_province['price_shipping']) + '" min="-999999" max="999999" class="form-control"></div>'
				// htmlEdit += '<div class="form-group"><input type="checkbox" checked="true"> &nbsp;' + item['district_name'] + '<input type="number" name="price_edit_' + (key+1) + '" value="' + item['price'] + '" min="0" max="999999" class="form-control"></div>'
			});
			htmlEdit += '</div>';

			html += htmlEdit;

			$(event.target).parents('section').find('form').attr('action',url_route);

			$(event.target).parents('section').find('#modalBodyEdit').html(html);

			$(event.target).parents('section').find("input[name='name_edit']").val(data.tranport_province['name']);
			$(event.target).parents('section').find("input[name='price_edit']").val(data.tranport_province['price_shipping']);
		}
	});
}

var remove_tranport_province = function(event) {
	var id_tranport = $(event.target).parents('.button').find('input').val();

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url : base_url + '/admin/tranports/' + id_tranport,
		type : 'DELETE',
		data : '',
		success : function(data){
			$(event.target).parents('.item').remove();
		}
	});
}