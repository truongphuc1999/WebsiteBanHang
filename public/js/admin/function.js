var remove_img = function(event) {
	$(event.target).parents(".product_img").remove();	
	// console.log("123")
}

var img_click = function(event){
	$(event.target).parents('.product_img').find('input').trigger('click');
}

var preview_img_upload = function(event,input){
	var reader = new FileReader();

	reader.onload = function (e) {
		$(event.target).parents('.product_img').find('img').attr('src',e.target.result);
		// console.log(e.target.result);
	};

	reader.readAsDataURL(input.files[0]);
	
	// $(event.target).parents('.product_img').find('img').attr('src',event.target.result);
}

var add_img = function(event){
	var content_current = $(event.target).parents("#product_img").html();
	var item = '<div class="product_img" style="width: 150px; float:left; position: relative; margin-right: 20px; margin-bottom: 20px;"><img width="100%" class="img" alt="img_product" name="" onclick="img_click(event);" src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" style="cursor: pointer;"><a onclick="remove_img(event)" class="btn btn-circle btn-danger" style="width:25px; height: 26px;text-align: center; line-height: 26px; border-radius: 50%; color: white; position: absolute;top: 0; right: 0"><b style="width: 10px; height: 10px;position: absolute;top:-2px;right: 30%;">-</b></a><input type="file" name="upload_images[]" value="{{ $product_image->name }}" id="image_{{ $key + 1 }}" style="display: none" onchange="preview_img_upload(event,this)"><input type="hidden" name="image_current[]" value="{{ $product_image->name }}"></div>';
	$(event.target).parents("#product_img").html(content_current + item);
}