var load_districts = function(event) {
	var id = event.target.value;

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url : base_url + '/dat-hang/load-districts',
		type : 'GET',
		data : {'province_id' : id},
		success : function(data){
			$('#district').html(data.districts);
			$('#district').prop("disabled", false);
		}
	});
}

var load_wards = function(event){
	var id = event.target.value;

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url : base_url + '/dat-hang/load-wards',
		type : 'GET',
		data : {'district_id' : id},
		success : function(data){
			$('#ward').html(data.wards);
			$('#ward').prop("disabled", false);
		}
	});
}