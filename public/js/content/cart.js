$(document).ready(function(){
	$('.quantity-minus').click(function(){
		var idInput = $(this).attr('id');
		$('#'+ 'quantity-' +idInput).trigger('change');
	});

	$('.quantity-plus').click(function(){
		var idInput = $(this).attr('id');
		$('#'+ 'quantity-' +idInput).trigger('change');
	});
});

var updateCart = function(event, rowId){
	var qty = $(event.target).val();

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url : base_url + '/gio-hang/update',
		type : 'POST',
		data : {'rowId': rowId, 'quantity': qty},
		success : function(data){
			if(data.countQty == qty){
				$('.countCart').html(data.count);
				$('.totalCart').html(data.total);
			}
			else{
				document.getElementById((event.target).value = data.countQty);
			}
		}
	});
}
	
var deleteCart = function(event, rowId){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url : base_url + '/gio-hang/delete',
		type : 'GET',
		data : {'rowId' : rowId},
		success : function(data){
			$('.countCart').html(data.count);
			$('.totalCart').html(data.total);

			if(data.count == 0){
				$('#cart-exist').hide();
				$('#cart-none').show();
			}else{
				$(event.target).parents("tr").remove();
			}
		}
	});
}

var destroyCart = function(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url : base_url + '/gio-hang/destroy',
		type : 'GET',
		success : function(data){
			$('.countCart').html(data.count);
			$('.totalCart').html(data.total);

			$('#cart-exist').hide();
			$('#cart-none').show();
		}
	});
}