// Mobile Navigation
function open_nav() {
    $("#nav-toggle").addClass("open");
    $("#viewport").addClass("slide");
    $(".overlay").addClass("show");
}
 
function close_nav() {
    $("#nav-toggle").removeClass("open");
    $("#viewport").removeClass("slide");
    $(".overlay").removeClass("show");
}

// Scroll đầu trang
var scroll_top = function(){
	$("html, body").animate({ scrollTop: 0 }, "slow");
};
// Datetime Picker
// $(function() {
// 	$( ".datepicker" ).datepicker({
// 		dateFormat : "dd/mm/yy"
// 	});
// });

// Number Control
var quantity_add = function(event){
	var value = event.target.parentElement.parentElement.getElementsByTagName("input")[0];
	if(value.value < 1)
		value.value = 1;
	else
		value.value = parseInt(value.value) + 1;
};
var quantity_sub = function(event){
	var value = event.target.parentElement.parentElement.getElementsByTagName("input")[0];
	if(value.value > 1)
		value.value = parseInt(value.value) - 1;
	else
		value.value = 1;
};

// Xem ảnh sau khi chọn
var upload_img_preview = function(event) {
	var files = event.target.files;
	var file = files[0];
	if (file) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$(event.target).parents(".image-preview").find("img").attr("src", e.target.result);
		};
		reader.readAsDataURL(file);
	}
}

// Remove ảnh trong custom image
var remove_file = function(event) {
	$(event.target).parents(".custom-image-upload").find("img.img-preview").attr("src", "");
	$(event.target).parents(".custom-image-upload").find("input[type=file]").val("");
}

// Check all table checkbox
$(document).ready(function() {
	$("#check-all").on("change", function() {
		if($(this).prop('checked') == true) {
			$(".table input[type=checkbox]").prop("checked", true);
		} else {
			$(".table input[type=checkbox]").prop("checked", false);
		}
	});
});

// Tooltip
$(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

// Định dạng số
function number_format (number, decimals, dec_point, thousands_sep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	s = '',
	toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec);
		return '' + Math.round(n * k) / k;
	};
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
};

// Định dạng tiền
var make_money_format = function(event) {
	var mn_val = event.target.value;
	$(event.target).val(number_format(mn_val));
};

// Convert to slug
function convertToSlug(str)
{
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

	// remove accents, swap ñ for n, etc
	var from = "äàáảãạăằắẳẵặâầấẩẫậđèéẻẽẹêềếểễệëìíỉĩịòóỏõọôồốổỗộơờớởỡợưỳýỷỹỵừứửữựïîöñç·/_,:;";
	var to   = "aaaaaaaaaaaaaaaaaadeeeeeeeeeeeeiiiiiooooooooooooooooouyyyyyuuuuuiionc------";
	for (var i=0, l=from.length ; i<l ; i++) {
	str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}

	str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
	.replace(/\s+/g, '-') // collapse whitespace and replace by -
	.replace(/-+/g, '-'); // collapse dashes

	return str;
}

//Custome Owl-Carousel
$('.owl-products').owlCarousel({
    // loop:true,
    margin:30,
    nav:true,
    navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
    // autoplay:true,
    dots:false,
    responsive:{
    	0:{
    		items:2,
    		nav:false
    	},
    	1000:{
    		items:4
    	}
    }
})

$('#owlSlider').owlCarousel({
    items:1
})

$('.owl-123').owlCarousel({
	nav:true,
	dots:false,
    items: 5
})