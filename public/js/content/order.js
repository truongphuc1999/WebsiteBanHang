var other_address = function(event) {

	if(document.getElementById("btnOtherAddress").innerHTML == "Quay lại"){
		$('#formAddressDefault').show();
		$('#formAddressOther').hide();
		$('#temp').val(1);
		document.getElementById("btnOtherAddress").innerHTML = "Địa chỉ khác";
	}else{
		$('#formAddressDefault').hide();
		$('#formAddressOther').show();
		$('#temp').val(2);
		document.getElementById("btnOtherAddress").innerHTML = "Quay lại";
	}
}

var cul_shipping = function(event) {
	var id = event.target.value;

	// console.log(id);
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url : base_url + '/dat-hang/load-cul-shipping',
		type : 'GET',
		data : { 'district_id' : id },
		success : function(data){
			$(event.target).parents('.container').find("strong[name='shipping']").html(data.price_shipping);

			// console.log(data.price_shipping);
			// console.log(data.total_price);

			$(event.target).parents('.container').find('#total').html(data.total_price);
		}
	});
}