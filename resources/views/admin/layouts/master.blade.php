<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Trang quản trị</title>

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="{{asset('css/admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('css/admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{asset('css/admin/bower_components/Ionicons/css/ionicons.min.css')}}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{asset('css/admin/AdminLTE.min.css')}}">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
  	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="{{asset('css/admin/skins/_all-skins.min.css')}}">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="{{asset('css/admin/bower_components/morris.js/morris.css')}}">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="{{asset('css/admin/bower_components/jvectormap/jquery-jvectormap.css')}}">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="{{asset('css/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="{{asset('css/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="{{asset('css/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

  	<link rel="stylesheet" href="{{asset('css/admin/style.css')}}">

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		@include('admin.layouts.header')
		@include('admin.layouts.sidebar')
		
		<div class="content-wrapper">
			@yield('content')
		</div>

		@include('admin.layouts.footer')
	</div>

	<!-- jQuery 3 -->
	<script src="{{asset('css/admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="{{asset('css/admin/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		
	<!-- Bootstrap 3.3.7 -->
	<script src="{{asset('css/admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<!-- Morris.js charts -->
	<script src="{{asset('css/admin/bower_components/raphael/raphael.min.js')}}"></script>
	<script src="{{asset('css/admin/bower_components/morris.js/morris.min.js')}}"></script>
	<!-- Sparkline -->
	<script src="{{asset('css/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
	<!-- jvectormap -->
	<script src="{{asset('css/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
	<script src="{{asset('css/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{asset('css/admin/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
	<!-- daterangepicker -->
	<script src="{{asset('css/admin/bower_components/moment/min/moment.min.js')}}"></script>
	<script src="{{asset('css/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	<!-- datepicker -->
	<script src="{{asset('css/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{asset('css/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
	<!-- Slimscroll -->
	<script src="{{asset('css/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
	<!-- FastClick -->
	<script src="{{asset('css/admin/bower_components/fastclick/lib/fastclick.js')}}"></script>
	<!-- AdminLTE App -->
	<script src="{{asset('js/admin/adminlte.min.js')}}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="{{asset('js/admin/pages/dashboard.js')}}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{asset('js/admin/demo.js')}}"></script>
	<!-- CKEditor -->
	<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
	<script> 
		CKEDITOR.replace( 'editor1', {
			filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
			filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
			filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
			filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
			filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
			filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
		} );
	</script>
	<script src="{{ asset('js/admin/function.js') }}"></script>
	<script src="{{ asset('js/admin/ajaxTranport.js') }}"></script>
	<script type="text/javascript">
		var base_url = {!! json_encode(url('/')) !!};
	</script>
</body>
</html>