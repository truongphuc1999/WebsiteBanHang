{{-- <!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  <link rel="stylesheet" href="{{asset('admin/css/login.css')}}">
</head>

<body>
  <div class="login">
    <div class="login-triangle"></div>

    <h2 class="login-header">Log in</h2>

    @if(count($errors) > 0)
      <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{$error}}</li>
        @endforeach
      </ul>
    @endif

    <form class="login-container" method="POST" action="{{url('admin/login')}}">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <p><input type="email" placeholder="Email" name="txtEmail"></p>
      <p><input type="password" placeholder="Password" name="txtPassword"></p>
      <p><input type="submit" value="Log in"></p>
    </form>
  </div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

</body>

</html> --}}

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Account Login</title>
<!-- for-mobile-apps -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- //for-mobile-apps -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
<link href="{{asset('admin/css/login.css')}}" rel="stylesheet" type="text/css" media="all" />
<script src="{{asset('admin/js/jquery-1.11.1.min.js')}}"></script> 
<!-- //js -->
<!-- chart -->
<!-- //chart -->
<script src="{{asset('admin/js/easyResponsiveTabs.js')}}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true   // 100% fit in a container
      });
    });
     </script>
</head>
<body>
<div class="content">
    <div class="main">
      <div class="profile-left wthree">
        <div class="sap_tabs">
        <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
          <ul class="resp-tabs-list">
            <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Đăng nhập</span></li>
            <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><h2><span>Đăng ký</span></h2></li>
            <div class="clear"> </div>
          </ul>     
          <div class="resp-tabs-container">
            <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
            
              <div class="login-top">
                <form method="POST" action="{{url('admin/login')}}">
                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                  <input type="text" name="txtEmail" class="email" placeholder="Email" required=""/>
                  <input type="password" name="txtPassword" class="password" placeholder="Password" required=""/>  
                  <input type="checkbox" id="brand" value="">
                  <label for="brand"><span></span> Nhớ mật khẩu</label>

                  <div class="login-bottom">
                  <ul>
                    <li>
                      
                        <input type="submit" value="LOGIN"/>
                      
                    </li>
                    <li>
                      <a href="#">Quên mật khẩu?</a>
                    </li>
                  <ul>
                  <div class="clear"></div>
                </div>
                </form>  
              </div>
            </div>
            <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
              <div class="login-top sign-top">
                <form action="#" method="post">
                  <input type="text" name="name" class="name active" placeholder="Your Name" required=""/>
                  <input type="text" name="email" class="email" placeholder="Email" required=""/>
                  <input type="text" name="password" class="phone" placeholder="Phone" required=""/>
                  <input type="password" name="password" class="password" placeholder="Password" required=""/>    
                  <input type="checkbox" id="brand1" value="">
                  <label for="brand1"><span></span> Remember me?</label>
                </form>
                <div class="login-bottom">
                  <ul>
                    <li>
                      <form action="#" method="post">
                        <input type="submit" value="SIGN UP"/>
                      </form>
                    </li>
                    <li>
                      <a href="#">Forgot password?</a>
                    </li>
                  <ul>
                  <div class="clear"></div>
                </div>  
              </div>
            </div>
          </div>  
        </div>
        <div class="clear"> </div>
      </div>
      <div class="social-icons w3agile">
        <ul> 
        <li class="ggp">Sign in with :</li>
          <li><a href="#"><span class="icons"></span><span class="text">Facebook</span></a></li>
          <li class="twt"><a href="#"><span class="icons"></span><span class="text">Twitter</span></a></li>
          <div class="clear"> </div>
        </ul> 
      </div>
      </div>
      <div class="clear"> </div>
  </div>  
  <p class="footer">&copy; 2016 Account Login Widget. All Rights Reserved | Design by <a href="http://w3layouts.com/"> W3layouts</a></p>
</div>
</body>
</html>
