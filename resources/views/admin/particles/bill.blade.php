@extends('admin.layouts.master')

@section('content')
<section class="content-header">
    <h1>
        Danh sách đơn hàng
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Bill</a></li>
        <li class="active">List</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    @if (Session::has('message'))
    <div class="alert alert-info"> {{ Session::get('message') }}</div>
    @endif
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-md-12">
                    <table id="myTable" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting col-md-1" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="" >Mã đơn hàng</th>
                                <th class="sorting_asc col-md-2" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="">Tên khách hàng</th>
                                <th class="sorting col-md-2" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Ngày đặt hàng</th>
                                {{-- <th class="sorting col-md-1" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Ngày đặt hàng</th>
                                <th>Email</th> --}}
                                <th>Trạng thái</th>
                                <th class="sorting col-md-1" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Action</th>
                                <th class="sorting col-md-2" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Xóa</th></tr>
                            </thead>
                            <tbody>
                                @foreach($bills as $bill)
                                <tr>
                                    <td>#{{ $bill->id }}</td>
                                    <td>{{ $bill->customer_name }}</td>
                                    <td>{{ $bill->bill_date }}</td>

                                    @if($bill->status == 0)
                                        <td>Đang chờ xác nhận</td>
                                    @elseif($bill->status == 1)
                                        <td>Đã xử lý</td>
                                    @elseif($bill->status == 2)
                                        <td>Đang giao hàng</td>
                                    @elseif($bill->status == 3)
                                        <td>Giao hàng thành công</td>
                                    @elseif($bill->status == 4)
                                        <td>Đã hủy</td>
                                    @endif

                                    <td><a href="{{ route('bills.edit',$bill->id) }}">Detail</a></td>
                                    <td>
                                        <form action="{{ route('bills.destroy', $bill->id) }}" method="post" id="formDelete">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <input type="submit" value="Delete" class="btn btn-danger"  onclick="return confirm('Bạn có thực sự muốn xóa?')">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop