@extends('admin.layouts.master')

@section('content')
  @if(Request::is('*/categories/create'))
  	<section class="content-header">
         <h1>
             Thêm danh mục
         </h1>
         <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
             <li><a href="#">Category</a></li>
             <li class="active">Add</li>
         </ol>
    </section>
    <section class="content">
           <form action="{{ route('categories.store') }}" method="POST">
             <input type="hidden" name='_token' value="{{csrf_token()}}">
             @if(count($errors) >0)
             <ul>
               @foreach($errors->all() as $error)
               <li class="text-danger">{{ $error }}</li>
               @endforeach
             </ul>
             @endif
             <div class="box">
               <div class="box-body row">

                <div class="form-group col-md-12">
                  <label>Tên danh mục</label>
                  <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                </div>
                <div class="form-group col-md-12">
                  <label>Đường dẫn</label>
                  <input type="text" name="slug" class="form-control"  value="{{ old('slug') }}">
                </div>
                      {{--  <div class="form-group col-md-12">
                           <label>Desc</label>
                           <textarea name="txtDesc" class="form-control">{{ old('txtDesc') }}</textarea>
                         </div> --}}
                         <div class="form-group col-md-12">
                          <label>Danh mục cha</label>
                          <select class="form-control" name="parent_id"  value="{{ old('parent_id') }}">
                            <option value="0">---</option>
                            @foreach($listCate as $cate)
                              <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group col-md-12">
                          <fieldset>  <legend>SEO:</legend>
                            Tiêu đề SEO <input type="text" name="meta_title" class="form-control"  value="{{ old('meta_title') }}">
                            Mô tả SEO <input type="text" name="meta_description" class="form-control"  value="{{ old('meta_description') }}">
                          </fieldset>
                        </div>
                      </div>
                      <div class="box-footer row">
                       <button type="submit" class="btn btn-success">
                         <i class="fa fa-save"></i>
                         <span>Lưu và trở lại</span>
                       </button>

                       @if(Session::has('message'))
                       <div class="arlert arlert-success">{{Session::get('message')}}</div>
                       @endif
                     </div>
                   </div>
                 </form>
    </section>
  @else
    <section class="content-header">
        <h1>
            Chỉnh sửa danh mục
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Category</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <form action="{{ route('postCategories.update', $category->id) }} {{-- {{ route('postCategories.update'), ['id' => $id] }} --}}" method="POST">
          {{-- <input type="hidden" name="_method" value="PUT"> --}}
            {{ csrf_field() }}
            @if(count($errors) >0)
                <ul>
                @foreach($errors->all() as $error)
                    <li class="text-danger">{{ $error }}</li>
                @endforeach
                </ul>
            @endif
            <div class="box">
                <div class="box-body row">
                    {{-- <input type="hidden" name="_id" value="{{ $category->id }}"> --}}
                    <div class="form-group col-md-12">
                        <label>Tên danh mục</label>
                        <input type="text" name="name" class="form-control" value="{{ $category->name }}">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Đường dẫn</label>
                        <input type="text" name="slug" class="form-control"  value="{{ $category->slug }}">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Danh mục cha</label>
                        <select class="form-control" name="parent_id">
                          <option value="0">---</option>
                            @foreach($listCate as $cate)
                              <option value="{{ $cate->id }}" 
                              {{-- @if ($cate->parent_id == $category->parent_id) 
                                selected="selected"
                              @endif --}}
                              @if($category->parent_id == $cate->id)
                                selected="selected" 
                              @endif
                              >{{ $cate->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                      <fieldset>  <legend>SEO:</legend>
                      Tiêu đề SEO <input type="text" name="meta_title" class="form-control"  value="{{ $category->meta_title }}">
                      Mô tả SEO <input type="text" name="meta_description" class="form-control"  value="{{ $category->meta_description }}">
                    </fieldset>

                    </div>
                </div>
                <div class="box-footer row">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        <span>Save and back</span>
                    </button>

                    @if(Session::has('message'))
                       <div class="arlert arlert-success">{{Session::get('message')}}</div>
                       @endif
                </div>
            </div>
        </form>
    </section>
  @endif
@stop