@extends('admin.layouts.master')

@section('content')
<section class="content-header">
	<h1>
		Cấu hình vận chuyển
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Configuration Tranport</li>
	</ol>
</section>
<section class="content">
		<div class="row">
			<div class="col-sm-4">
				<h4>Phí vận chuyển</h4>
				<p>Thêm phí vận chuyển cho các khu vực khác nhau</p>
				<a href="" class="btn btn-success"  data-toggle="modal" data-target="#addTranportProvince">Thêm khu vực vận chuyển</a>
				<!-- Modal thêm khu vực vận chuyển -->
				<form action="{{ route('tranports.store') }}" method="POST">
					{{csrf_field()}}
				<div class="modal fade" id="addTranportProvince" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Thêm khu vực vận chuyển</h4>
							</div>
							<div class="modal-body">
								<select name="province" id="province" value="" class="custom-select form-control" style="margin-bottom: 10px;">
									<option value="">— Chọn khu vực vận chuyển —</option>
									<option value="0">Tất cả khu vực</option>
									@foreach($province as $item)
										<option value="{{ $item->id }}">{{ $item->name }}</option>
									@endforeach
								</select>
								<label for="">Phí vận chuyển: </label> &nbsp;
								<input type="number" name="price" value="0" min="0" max="999999">
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
								<button class="btn btn-success"{{--  onclick="save_province_tranport(event);"  --}}>Lưu</button>
							</div>
						</div>

					</div>
				</div>
				</form>
			</div>
			<div class="col-sm-8">
				<h4>Khu vực vận chuyển</h4>
				@foreach($tranport_province as $item)
				<div class="item">
					<div class="province">
						{{ $item->province->name }}
					</div>
					<div class="price">
						Phí giao hàng: <b>{{ $item->price_shipping }}<u>đ</u></b>
					</div>
					<div class="button">
						<div><a class="btn btn-danger" onclick="remove_tranport_province(event);">Xóa khu vực</a></div>
						<input type="hidden" value="{{ $item->id }}">
						<div><a href="" class="btn btn-default" data-toggle="modal" data-target="#editTranportProvince" onclick="get_tranport_province(event);" >Chỉnh sửa phí vận chuyển</a></div>
					</div>
				</div>
				@endforeach
			</div>

			<!-- Modal Chỉnh sửa phí vận chuyển-->
			<form action="" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<div class="modal fade" id="editTranportProvince" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Chỉnh sửa phí vận chuyển</h4>
							</div>
							<div class="modal-body" id="modalBodyEdit">
								<div class="form-group">
									<label for="">Tên phương thức vận chuyển: </label> &nbsp;
									<input type="text" name="name_edit" value="" class="form-control" >
								</div>

								<div class="form-group">
									<label for="">Phí vận chuyển: </label> &nbsp;
									<input type="number" name="price_edit" value="0" min="0" max="999999" class="form-control">
								</div>

								<label for="">Giá điều chỉnh:</label>
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
								<button class="btn btn-success">Lưu</button>
							</div>
						</div>

					</div>
				</div>
			</form>
		</div>
	</section>
@stop