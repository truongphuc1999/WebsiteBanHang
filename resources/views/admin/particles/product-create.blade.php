@extends('admin.layouts.master')

@section('content')
  @if(Request::is('*/products/create'))
  	<section class="content-header">
         <h1>
             Thêm sản phẩm
         </h1>
         <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
             <li><a href="#">Products</a></li>
             <li class="active">Add</li>
         </ol> 

         @if(Session::has('message'))
          <div class="alert alert-success">{{Session::get('message')}}</div>
         @endif

    </section>
    <section class="content">
           <form action="{{ route('products.store') }}" enctype="multipart/form-data" method="POST">
             <input type="hidden" name='_token' value="{{csrf_token()}}">
             @if(count($errors) >0)
             <ul>
               @foreach($errors->all() as $error)
               <li class="text-danger">{{ $error }}</li>
               @endforeach
             </ul>
             @endif
             <div class="box">
               <div class="box-body row">

                <div class="form-group col-md-12">
                  <label>Tên sản phẩm</label>
                  <input type="text" name="name" class="form-control" value="{{ old('txtName') }}">
                </div>
                <div class="form-group col-md-12">
                  <label>Thông tin sản phẩm</label>
                  <textarea name="description" class="" id="editor1" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group col-md-12">
                  <label>Giá</label>
                  <input type="text" name="price" class="form-control"  value="">
                </div>
                <div class="form-group col-md-12">
                  <label>Giảm giá (%)</label>
                  <input type="text" name="percent_sale" class="form-control"  value="">
                </div>
                <div class="form-group col-md-12">
                  <label>Số lượng</label>
                  <input type="text" name="quantity" class="form-control"  value="">
                </div>
                <div class="form-group col-md-12">
                	<label>Đường dẫn</label>
                	<input type="text" name="slug" class="form-control"  value="">
                </div>
                <div class="form-group col-md-12">
                	<label>Danh mục sản phẩm</label>
                	<select class="form-control" name="category_id"  value="{{ old('parent_id') }}">
                		<option value="0">---</option>
                		@foreach($listCate as $cate)
                		<option value="{{ $cate->id }}">{{ $cate->name }}</option>
                		@endforeach
                	</select>
                </div>
                <div class="form-group col-md-12">
                  <label>Thêm hình ảnh</label>
                  <input type="file" name="upload_images[]" class="form-control"  value="" multiple>
                </div>
                <div class="form-group col-md-12">
                	<fieldset>  <legend>SEO:</legend>
                		Tiêu đề SEO <input type="text" name="meta_title" class="form-control"  value="{{ old('meta_title') }}">
                		Mô tả SEO <input type="text" name="meta_description" class="form-control"  value="{{ old('meta_description') }}">
                	</fieldset>
                </div>
            </div>
            <div class="box-footer row">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-save"></i>
            		<span>Lưu và trở lại</span>
            	</button>
            </div>
        </div>
    </form>
    </section>
  @else
    <section class="content-header">
         <h1>
             Cập nhật thông tin sản phẩm
         </h1>
         <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
             <li><a href="#">Products</a></li>
             <li class="active">Update</li>
         </ol> 
    </section>
    <section class="content">
           <form action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data" method="POST">
             <input type="hidden" name='_token' value="{{csrf_token()}}">
             @if(count($errors) >0)
             <ul>
               @foreach($errors->all() as $error)
               <li class="text-danger">{{ $error }}</li>
               @endforeach
             </ul>
             @endif
             <div class="box">
               <div class="box-body row">

                <div class="form-group col-md-12">
                  <label>Tên sản phẩm</label>
                  <input type="text" name="name" class="form-control" value="{{ $product->name }}">
                </div>
                <div class="form-group col-md-12">
                  <label>Thông tin sản phẩm</label>
                  <textarea name="description" class="" id="editor1" cols="30" rows="10">{{ $product->description }}</textarea>
                </div>
                <div class="form-group col-md-12">
                  <label>Giá</label>
                  <input type="text" name="price" class="form-control"  value="{{ $product->price }}">
                </div>
                <div class="form-group col-md-12">
                  <label>Giảm giá (%)</label>
                  <input type="text" name="percent_sale" class="form-control"  value="{{ $product->percent_sale }}">
                </div>
                <div class="form-group col-md-12">
                  <label>Số lượng</label>
                  <input type="text" name="quantity" class="form-control"  value="{{ $product->quantity }}">
                </div>
                <div class="form-group col-md-12">
                  <label>Đường dẫn</label>
                  <input type="text" name="slug" class="form-control"  value="{{ $product->slug }}">
                </div>
                <div class="form-group col-md-12">
                  <label>Danh mục sản phẩm</label>
                  <select class="form-control" name="category_id"  value="">
                    <option value="0">---</option>
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}" {{ $category->id == $product->category_id ? 'selected' : "" }} >{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-md-12">
                  <label>Hình ảnh</label>
                  <div id="product_img">
                    @foreach($product_images as $key => $product_image)
                    <div class="product_img" style="width: 150px; float:left; position: relative; margin-right: 20px; margin-bottom: 20px;">
                        <img width="100%" class="img" alt="img_product" name="image_{{ $key + 1 }}" onclick="img_click(event);" src="{{ asset('images/content/products/' . $product_image->name) }}" style="cursor: pointer;">
                        {{-- Cho code them mau sac --}}
                        <a onclick="remove_img(event)" class="btn btn-circle btn-danger" style="width:25px; height: 26px;text-align: center; line-height: 26px; border-radius: 50%; color: white; position: absolute;top: 0; right: 0"><b style="width: 10px; height: 10px;position: absolute;top:-2px;right: 30%;">-</b></a>
                      <input type="file" name="upload_images[]" value="{{ $product_image->name }}" id="image_{{ $key + 1 }}" style="display: none" onchange="preview_img_upload(event,this)">
                      <input type="hidden" name="image_current[]" value="{{ $product_image->name }}">
                    </div>
                    @endforeach
                    <button style="margin-top: 60px;" onclick="add_img(event);" class="btn btn-primary" type="button">Thêm ảnh</button>
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <fieldset>  <legend>SEO:</legend>
                    Tiêu đề SEO <input type="text" name="meta_title" class="form-control"  value="{{ $product->meta_title }}">
                    Mô tả SEO <input type="text" name="meta_description" class="form-control"  value="{{ $product->meta_description }}">
                  </fieldset>
                </div>
            </div>
            <div class="box-footer row">
              <button type="submit" class="btn btn-success">
                <i class="fa fa-save"></i>
                <span>Lưu và trở lại</span>
              </button>

              @if(Session::has('message'))
              <div class="arlert arlert-success">{{Session::get('message')}}</div>
              @endif
            </div>
        </div>
    </form>
    </section>
  @endif
@stop