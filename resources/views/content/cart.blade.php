@extends('content.layouts.master')

@section('content')
<div class="container">
	@if(Session::has('popup'))
		<script>
			alert('{{ Session::get('popup') }}');
		</script>
	@endif
	@if($count > 0)
	<div id="cart-exist" class="row">
		<div class="col-md-8">
			<div class="block block-cart my-md-5 my-4">
				<table class="table table-bordered mb-3">
					<tbody>
						
						<form action="{{ route('updateCart') }}" method="POST">
							{{ csrf_field() }}
						@foreach($cart as $item)
						
						<tr>
							<td width="120">
								<a data-fancybox="gallery" href="{{ asset('images/content/products/' . $item->image) }}">
									<img src="{{ asset('images/content/products/' . $item->options->img ) }}" width="100">
								</a>
							</td>
							<td>
								<div class="mb-1">{{$item->name}}</div>
								<div class="mb-3">
									{{-- Chất liệu: <strong>Sứ</strong>&emsp;
									Bộ: <strong>10 cái</strong>&emsp;
									Kích thước: <strong>Lớn</strong>&emsp; --}}
								</div>
								<div class="form-inline">
									<div class="custom-number custom-number-small">
										<div class="input-group">
											{{-- <span class="input-group-btn">
												<a href="{{url("cart?product_id=$item->id&decrease=1")}}"><button type="button" class="btn quantity-minus" onclick="quantity_sub(event);">-</button></a>
											</span>
											<input type="text" id="quantity" name="quantity" value="{{ $item->qty }}" min="1" max="1000" onchange="update({{$item->rowid}})">
											<span class="input-group-btn">
												<a href="{{url("cart?product_id=$item->id&increment=1")}}"><button type="button" class="btn quantity-plus" onclick="quantity_add(event);">+</button></a>
											</span> --}}

											<span class="input-group-btn">
												<button type="button" id="{{$item->id}}" class="btn quantity-minus" onclick="quantity_sub(event)">-</button>
											</span>
											
											<input type="text" class="quantity" id="quantity-{{$item->id}}" name="quantity" value="{{ $item->qty }}" min="1" max="1000" onchange="updateCart(event,'{{$item->rowId}}')">
											<span class="input-group-btn">
												<button type="button" id="{{$item->id}}" class="btn quantity-plus" onclick="quantity_add(event)">+</button>
											</span>
										</div>
									</div>&emsp;
									<span class="text-strong">Giá: {{ number_format($item->price - ($item->price / 100 * $item->percent_sale),0,",",".") }} <u>đ</u></span><i class="symbol symbol-vnd"></i>
								</div>
							</td>
							<td width="50">
								<button type="button" id="delete-{{$item->id}}" onclick="deleteCart(event,'{{$item->rowId}}');" class="btn btn-trash">Xóa</button>
							</td>
						</tr>

						<p class="cart_count"></p>
						
						@endforeach
					</form>
					</tbody>
				</table>
				<div class="block-footer text-right px-0">
					<button class="btn btn-red-outline" onclick="destroyCart()">Hủy giỏ hàng</button>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<aside class="block-cart my-md-5 mb-5">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td>Tổng số</td>
							<td align="right"><span class="text-strong countCart">{{ $count }}</span> sản phẩm</td>
						</tr>
						<tr>
							<td>Thành tiền</td>
							<td align="right"><span class="text-strong totalCart">{{$totalPrice }}</span> <u>đ</u><i class="symbol symbol-vnd"></i></td>
						</tr>
					</tbody>
				</table>
				<div class="block-footer px-0">
					<a href="{{ route('getOrder') }}" class="btn btn-primary btn-block">Đặt hàng</a>
				</div>
			</aside>
		</div>
	</div>
	@endif
	<div id="cart-none" class="py-5 text-center" {{Cart::count() > 0 ? 'style=display:none' : ""}}>
		<h3>Không có sản phẩm nào trong giỏ hàng</h3>
		<a href="{{route('home')}}"><button>Tiếp tục mua hàng</button></a>
	</div>
</div>
@endsection