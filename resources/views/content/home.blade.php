@extends('content.layouts.master')

@section('content')
	@include('content.particles.banner')
	@include('content.particles.policy')

	<div class="block-main-primary py-4">
		@include('content.particles.products')
		@include('content.particles.flash-sale')
	</div>
@stop