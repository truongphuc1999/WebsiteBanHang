<!-- START FOOTER -->
<footer>
	<div class="footer-top">
		<div class="container d-block d-lg-flex">
			<div class="registe-news">
				<div class="block-title">
					Đăng ký nhận bản tin AZweb
				</div>
				<div class="block-content">
					Cập nhật thông tin khuyến mãi nhanh nhất <br>
					Hưởng quyền lợi giảm giá riêng biệt
				</div>
			</div>
			<form action="">
				<div class="input-group mb-3">
					<input type="text" class="form-control" placeholder="Email của bạn">
					<div class="input-group-btn">
						<button class="btn btn-outline-secondary" type="button">GỬI</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="footer-middle">
		<div class="container">
			<div class="row">
				<div class="col-6 col-sm-6 col-lg-3">
					<div class="item line-right">
						<div class="block-title">
							AZWEB
						</div>
						<ul>
							<li><a href="">Giới thiệu về Azweb</a></li>
							<li><a href="">Quy chế hoạt động Sàn TMĐT</a></li>
							<li><a href="">Điều khoản và điều kiện giao dịch</a></li>
							<li><a href="">Thông báo từ AZweb</a></li>
							<li><a href="">Liên hệ với AZweb</a></li>
							<li><a href="">Trung tâm hỗ trợ khách hàng</a></li>
							<li><a href="">Đăng ký mở gian hàng</a></li>
						</ul>
					</div>
				</div>
				<div class="col-6 col-sm-6 col-lg-3">
					<div class="item line-right">
						<div class="block-title">
							CHĂM SÓC KHÁCH HÀNG
						</div>
						<ul>
							<li><a href="">Kiểm tra đơn hàng</a></li>
							<li><a href="">Chính sách bảo mật thanh toán</a></li>
							<li><a href="">Chính sách định danh khách hàng</a></li>
							<li><a href="">Chính sách thanh toán</a></li>
							<li><a href="">Chính sách giao hàng</a></li>
							<li><a href="">Chính sách đổi trả</a></li>
							<li><a href="">Hướng dẫn gửi trả hàng</a></li>
							<li><a href="">Chính sách bảo hành</a></li>
							<li><a href="">Câu hỏi thường gặp</a></li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 pr-0">
					<div class="item line-right">
						<div class="block-title">
							TÀI KHOẢN
						</div>
						<ul>
							<li><a href="">Đăng nhập</a></li>
							<li><a href="">Đăng ký</a></li>
							<li><a href="">Chính sách bảo mật và chia sẻ thông tin</a></li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3">
					<div class="item">
						<div class="block-title">
							LIKE ĐỂ NHẬN ƯU ĐÃI MỚI NHẤT
						</div>
						<div class="block-link-facebook">
							<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fthietkeweb24.vn%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="230px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="footer-bottom">
		<div class="container">
			<div class="block-top d-block d-lg-flex justify-content-between">
				<div class="block-right">
					<div class="block-title">
						CÔNG TY TNHH THIẾT KẾ WEB AZWEB
					</div>
					<div class="block-content">
						<ul>
							<li><strong>Địa chỉ: </strong>97/15 Hồng Lạc, Phường 10, Quận Tân Bình, TP Hồ Chí Minh</li>
							<li><strong>Điện thoại: </strong>(84.8) 3864 1235</li>
							<li><strong>Fax: </strong>(84.8) 39 71 81 56</li>
							<li><strong>Email: </strong>info@web24.vn</li>
						</ul>
					</div>
				</div>
				<div class="block-left">
					<img src="{{asset('images/content/footer/1.png')}}" alt="">
					<img src="{{asset('images/content/footer/2.png')}}" alt="">
				</div>
			</div>
			<hr>
			<div class="block-bottom">
				&copy; 2017 - Bản quyền của Công Ty TNHH AZweb AZweb.com.vn <br>
				Giấy chứng nhận Đăng ký Kinh doanh số 0909532909 do sở Kế hoạch và Đầu tư Thành Phố Hồ Chí Minh cấp ngày 06/01/2010
			</div>
		</div>
	</div>
</footer>
<!-- END FOOTER -->

<!-- START CONTENT NAV MOBILE -->
<nav id="nav-toggle" class="scrollbar">
	<div class="top">
		<img src="images/header/logo.png" alt="">
	</div>
	<form action="search.html" method="GET">
		<div class="form-group">
			<div class="input-group">
				<input type="search" class="form-control" name="keywords" placeholder="Tìm kiếm" value="">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default" onclick="open_nav()">
						<svg id="i-search" viewBox="0 0 32 32" width="22" height="22" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
							<circle cx="14" cy="14" r="12"></circle>
							<path d="M23 23 L30 30"></path>
						</svg>
					</button>
				</span>
			</div>
		</div>
	</form>
	<div class="middle">
		<ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="pill" href="#pills-menu" role="tab" aria-controls="pills-menu" aria-selected="true">
					<i class="fa fa-list-ul fa-lg"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#pills-auth" role="tab" aria-controls="pills-auth" aria-selected="false">
					<i class="fa fa-user-circle-o fa-lg"></i>
				</a>
			</li>
			<li class="nav-item close-nav">
				<a class="nav-link" onclick="close_nav()">
					<svg id="i-close" viewBox="0 0 32 32" width="15" height="15" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.5">
						<path d="M2 30 L30 2 M30 30 L2 2"></path>
					</svg>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade show active" id="pills-menu" role="tabpanel">
				<div class="item single">
					<a href="index.html">
						<i class="fa fa-angle-right"></i>&emsp;Trang chủ
					</a>
				</div>
				<div class="item single">
					<a href="">
						<i class="fa fa-angle-right"></i>&emsp;Giới thiệu
					</a>
				</div>
				<div class="item">
					<a data-toggle="collapse" data-parent="#pills-menu" href="#menu-item-1" aria-expanded="false" class="collapsed">
						<i class="fa fa-angle-right"></i>&emsp;Sản phẩm
					</a>
					<ul id="menu-item-1" class="collapse" role="tabpanel">
						<li><a href="products.html">Danh mục sản phẩm</a></li>
						<li><a href="products.html">Danh mục sản phẩm</a></li>
						<li><a href="products.html">Danh mục sản phẩm</a></li>
						<li><a href="products.html">Danh mục sản phẩm</a></li>
						<li><a href="products.html">Tất cả sản phẩm</a></li>
					</ul>
				</div>
				<div class="item">
					<a data-toggle="collapse" data-parent="#pills-menu" href="#menu-item-2" aria-expanded="false" class="collapsed">
						<i class="fa fa-angle-right"></i>&emsp;Tin tức
					</a>
					<ul id="menu-item-2" class="collapse" role="tabpanel">
						<li><a href="news.html">Chuyên mục</a></li>
						<li><a href="news.html">Chuyên mục</a></li>
						<li><a href="news.html">Chuyên mục</a></li>
						<li><a href="news.html">Chuyên mục</a></li>
						<li><a href="news.html">Tất cả tin tức</a></li>
					</ul>
				</div>
				<div class="item single">
					<a href="#">
						<i class="fa fa-angle-right"></i>&emsp;Dịch vụ
					</a>
				</div>
				<div class="item single">
					<a href="#">
						<i class="fa fa-angle-right"></i>&emsp;Liên hệ
					</a>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-auth" role="tabpanel">
				<div class="auth">
					<div class="media">
						<img class="mr-3" src="images/user.png">
						<div class="media-body">
							<h5 class="auth-name">Võ Trường Phúc</h5>
							phucproteam@gmail.com
						</div>
					</div>
				</div>
				<div class="item">
					<a href=""><i class="fa fa-dashboard fa-fw fa-lg"></i>&emsp;Trang quản trị</a>
				</div>
				<div class="item">
					<a href=""><i class="fa fa-user-circle-o fa-fw fa-lg"></i>&emsp;Hồ sơ cá nhân</a>
				</div>
				<div class="item">
					<a href=""><i class="fa fa-history fa-fw fa-lg"></i>&emsp;Lịch sử mua hàng</a>
				</div>
				<div class="item">
					<a href=""><i class="fa fa-heart fa-fw fa-lg"></i>&emsp;Danh sách yêu thích</a>
				</div>
				<div class="item">
					<a href=""><i class="fa fa-sign-out fa-fw fa-lg"></i>&emsp;Đăng xuất</a>
				</div>
			</div>
		</div>
	</div>
</nav>
<!-- END CONTENT NAV MOBILE -->