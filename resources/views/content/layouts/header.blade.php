<!-- START NAV MOBILE -->
<nav class="navigation nav-mobile">
	<div class="left">
		<button type="button" class="btn btn-default" onclick="open_nav()">
			<svg id="i-menu" viewBox="0 0 32 32" width="22" height="22" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
				<path d="M4 8 L28 8 M4 16 L28 16 M4 24 L28 24"></path>
			</svg>
		</button>
	</div>
	<div class="center">
		<a href="index.html">
			<img src="images/header/logo.png" alt="">
		</a>
	</div>
	<div class="right">
		<a href="cart.html">
			<img src="images/cart-icon.png" alt="">
			<span class="cart_count">0</span>
		</a>
	</div>
</nav>
<!-- END NAV MOBILE -->

<!-- START HEADER -->
<header class="d-none d-lg-block">
	<div class="header-top">
		<div class="container d-flex justify-content-between">
			<div class="block-left">
				<div class="item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
				<div class="item"><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></div>
				<div class="item"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></div>
				<div class="item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
			</div>
			<div class="block-right">
				<a href="">Theo dõi đơn hàng</a>
				<span>|</span>
				@if(Auth::check())
					<a href="{{route('login')}}">{{ Auth::user()->name }}</a>&emsp;
					<a href="{{route('logout')}}">Đăng xuất</a>
				@else
					<a href="{{route('login')}}">Đăng nhập</a>
					<a href="#"></a>
					&nbsp;
					<a href="{{ route('loginSocial','facebook') }}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					&nbsp;
					<a href="{{ route('loginSocial','google') }}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
				@endif
			</div>
		</div>
	</div>
	<div class="container">
		<div class="header-bottom d-flex">
			<div class="block-left">
				<a href="{{ route('home') }}"><img src="{{asset('images/content/header/logo.png')}}" alt=""></a>
				<div class="skew"></div>
			</div>
			<div class="block-right d-flex justify-content-between">
				<div class="search-bar">
					<form action="">
						<div class="input-group">
							<div class="input-group-prepend">
								<button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Danh mục sản phẩm</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="products.html">Sản phẩm 1</a>
									<a class="dropdown-item" href="products.html">Sản phẩm 2</a>
									<a class="dropdown-item" href="products.html">Sản phẩm 3</a>
									<div role="separator" class="dropdown-divider"></div>
									<a class="dropdown-item" href="products.html">Sản phẩm đổi trả</a>
								</div>
							</div>

							<input type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">

							<div class="input-group-append">
								<button class="btn" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
						</div>
					</form>
				</div>
				<div class="cart">
					<a href="{{ route('cart') }}"><img src="{{asset('images/content/header/icon-cart.png')}}" alt=""></a>
					<span class="cart-popup">
						<strong>{{ Cart::count() }}</strong>
					</span>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- END HEADER -->