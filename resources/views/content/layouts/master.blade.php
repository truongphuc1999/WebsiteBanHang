<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Trang chủ - Good Shop</title>

	<!-- Jquery 3.3.1 -->
	<script src="{{asset('libs/jquery/3.3.1/jquery-3.3.1.min.js')}}"></script>

	<!-- Bootstrap 4.0 -->
	<link rel="stylesheet" href="{{asset('libs/bootstrap/4.0/bootstrap.min.css')}}">
	<script src="{{asset('libs/bootstrap/4.0/popper.min.js')}}"></script>
	<script src="{{asset('libs/bootstrap/4.0/bootstrap.min.js')}}"></script>

	<!-- OwlCarousel2 2.3.3 -->
	<link rel="stylesheet" href="{{asset('libs/OwlCarousel2/2.3.3/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('libs/OwlCarousel2/2.3.3/owl.theme.default.min.css')}}">
	<script src="{{asset('libs/OwlCarousel2/2.3.3/owl.carousel.min.js')}}"></script>

	<!-- Xzoom  -->
	<link rel="stylesheet" href="{{asset('libs/xZoom/xzoom.css')}}">
	<script src="{{asset('libs/xZoom/foundation.min.js')}}"></script>
	<script src="{{asset('libs/xZoom/setup.js')}}"></script>
	<script src="{{asset('libs/xZoom/xzoom.min.js')}}"></script>

	<!-- Font Awesome -->
	<link href="{{asset('libs/fontawesome/4.7.0/css/font-awesome.min.css')}}" rel="stylesheet">

	<!-- Font Roboto -->
	<link href="https://fonts.googleapis.com/css?family=Roboto&amp;subset=latin-ext,vietnamese" rel="stylesheet">

	<!-- StyleSheet -->
	<link rel="stylesheet" href="{{asset('css/content/components.css')}}">
	<link rel="stylesheet" href="{{asset('css/content/default.css')}}">
	<link rel="stylesheet" href="{{asset('css/content/mobilebar.css')}}">

	<link rel="stylesheet" href="{{asset('css/content/style.css')}}">
</head>
<body>
	@include('content.layouts.header')

	<main>
		@yield('content')
	</main>
	
	@include('content.layouts.footer')

	<script src="{{asset('js/content/functions.js')}}"></script>
	<script src="{{asset('js/content/cart.js')}}"></script>
	<script src="{{asset('js/content/areas.js')}}"></script>
	<script src="{{asset('js/content/order.js')}}"></script>
	<script type="text/javascript">
		var base_url = {!! json_encode(url('/')) !!};
	</script>
</body>
</html>