@extends('content.layouts.master')

@section('content')
<div class="container">
<form action="{{ route('postOrder') }}" method="POST">
	{{csrf_field()}}
	<div class="row">
		<div class="col-md-8">
				<div class="block default__block-checkout default__block-primary mt-md-5 my-4">
					<div class="block-header">
						<h5 class="block-title">Thông tin người nhận</h5>
					</div>
					<div class="block-body">
						{{-- <div class="row" id="chooseAddress">
							<div class="col-md-12">
								<table id="myTable" class="table table-hover" role="grid" aria-describedby="example2_info"  style="font-size: 14px;">
									<thead>
										<tr role="row">
											<th class="sorting_asc" width="25%">Tên khách hàng</th>
											<th class="sorting" width="15%">Số điện thoại</th>
											<th class="sorting">Email</th>
											<th class="sorting">Địa chỉ</th>
											<th class="sorting"></th>
										</tr>
									</thead>
									<tbody>
										@foreach($customers as $key => $item)
										<tr>
											<td>{{ $item->name }}</td>
											<td>{{ $item->phone }}</td>
											<td>{{ $item->email }}</td>
											<td>{{ $item->address }}</td>
											<td><input type="radio" name="rdoCustomer" value="{{ $item->id }}" {{ $item->status == 1 ? "checked" : "" }} onchange="cul_shipping(event)"></td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div> --}}
						@if($customers != "")
						<div class="row" id="formAddressDefault">
							<div class="col-md-6">
								<div class="form-group">
									<label>Họ tên <span class="required">(*)</span></label>
									<input type="text" name="name1" class="form-control" value="{{ $customers->name }}" required="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Số điện thoại <span class="required">(*)</span></label>
									<input type="text" name="phone1" class="form-control" value="{{ $customers->phone }}" required="">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Email <span class="required">(*)</span></label>
									<input type="email" name="email1" class="form-control" value="{{ $customers->email }}" required="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tỉnh thành <span class="required">(*)</span></label>
									<select name="province1" class="custom-select form-control" onchange="load_districts(event)" required="" disabled>
										<option value="">— Chọn Tỉnh/Thành phố —</option>
										@foreach($provinces as $province)
										<option value="{{ $province->id }}" {{ $province->id == $customers->province ? "selected" : "" }} >{{ $province->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Quận huyện <span class="required">(*)</span></label>
									<select id="district1" name="district1" class="custom-select form-control" onchange="load_wards(event); cul_shipping(event);" required="" disabled>
										<option value="">— Chọn Quận/Huyện —</option>
										@foreach($districts as $district)
											<option value="{{ $district->id }}" {{ $district->id == $customers->district ? "selected" : "" }} >{{ $district->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Phường xã <span class="required">(*)</span></label>
									<select id="ward1" name="ward1" class="custom-select form-control" required="" disabled>
										<option value="">— Chọn Phường/Xã —</option>
										@foreach($wards as $ward)
											<option value="{{ $ward->id }}" {{ $ward->id == $customers->ward ? "selected" : "" }} >{{ $ward->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Số nhà, Tên đường <span class="required">(*)</span></label>
									<input type="text" name="address1" class="form-control" value="{{ $customers->address }}" required="">
								</div>
							</div>
						</div>
						<div class="form-group">
							<button id="btnOtherAddress" class="btn default__btn-primary" type="button" onclick="other_address(event)">Địa chỉ khác</button>
						</div>

						@endif
						<input type="hidden" id="temp" name="temp" value="1">

						<div class="row" id="formAddressOther" {{$customers != "" ? 'style=display:none' : ""}} >
							<div class="col-md-6">
								<div class="form-group">
									<label>Họ tên <span class="required">(*)</span></label>
									<input type="text" name="name" class="form-control" value="" {{ Auth::check() ? "" : "required" }}>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Số điện thoại <span class="required">(*)</span></label>
									<input type="text" name="phone" class="form-control" value="" {{ Auth::check() ? "" : "required" }}>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Email <span class="required">(*)</span></label>
									<input type="email" name="email" class="form-control" value="" {{ Auth::check() ? "" : "required" }}>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tỉnh thành <span class="required">(*)</span></label>
									<select name="province" class="custom-select form-control" onchange="load_districts(event)">
										<option value="">— Chọn Tỉnh/Thành phố —</option>
										@foreach($provinces as $province)
										<option value="{{$province->id}}">{{ $province->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Quận huyện <span class="required">(*)</span></label>
									<select id="district" name="district" class="custom-select form-control" onchange="load_wards(event); cul_shipping(event);" required="" disabled>
										<option value="">— Chọn Quận/Huyện —</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Phường xã <span class="required">(*)</span></label>
									<select id="ward" name="ward" class="custom-select form-control" required="" disabled>
										<option value="">— Chọn Phường/Xã —</option>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Số nhà, Tên đường <span class="required">(*)</span></label>
									<input type="text" name="address" class="form-control" value="{{ Auth::check() ? " " : "" }}" required="">
								</div>
							</div>
						</div>
						@if($customers == "")
						<div class="form-group">
							<a href="{{ route('login') }}"><button class="btn default__btn-primary" type="button" onclick="">Thanh toán nhanh hơn</button></a>
						</div>
						@endif
					</div>
				</div>


				{{-- <div class="block default__block-checkout default__block-primary mt-md-5 my-4">
					<div class="block-header">
						<h5 class="block-title">Thanh toán</h5>
					</div>
					<div class="block-body">
						<div class="form-group">
							<label class="custom-control custom-ticker custom-radio d-block">
								<input type="radio" name="pay_type" value="transfer" class="custom-control-input" checked="">
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description">Thanh toán khi nhận hàng (COD)</span>
							</label>
						</div>


					</div>
				</div> --}}
		</div>

		<div class="col-md-4">
			<aside class="block default__block-checkout default__block-primary my-md-5 mb-5">
				<div class="block-header">
					<h5 class="block-title">&emsp;</h5>
				</div>
				<div class="block-body scrollbar">
					@foreach($cart as $item)
					<div class="media">
						<a data-fancybox="gallery" href="">
							<img src="{!! asset('images/content/products/'.$item->options->img) !!}" width="60" class="mr-3">
						</a>
						<div class="media-body lh-default">
							<a href="{{-- {{route('productDetail', $item->options->slug)}} --}}" target="_blank">
								{{ $item->name }}
							</a>

							<div class="flex-row">
								<div class="flex-left">Số lượng:</div>
								<div class="flex-right"><strong>{{ $item->qty }}</strong></div>
							</div>
							<div class="flex-row">
								<div class="flex-left">Đơn giá:</div>
								<div class="flex-right"><strong>{{ number_format($item->price,0,",",".") }}</strong><i class="sb sb-vnd"></i></div>
							</div>
							<hr class="my-2">
							<div class="flex-row">
								<div class="flex-left">Thành tiền:</div>
								<div class="flex-right"><strong class="c-red">{{ number_format($item->price * $item->qty,0,",",".") }}</strong><i class="sb sb-vnd"></i></div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</aside>

			<aside class="block default__block-checkout default__block-primary my-md-5 mb-5">
				<div class="block-header"></div>
				<div class="block-body">

					<div class="flex-row">
						<div class="flex-left">Tổng số</div>
						<div class="flex-right"><strong class="c-red">{{ Cart::count() }}</strong> sản phẩm</div>
					</div>
					<div class="flex-row">
						<div class="flex-left">Tạm tính</div>
						<div class="flex-right"><strong class="c-red">{{ Cart::subtotal(0,',','.') }}</strong><i class="sb sb-vnd"></i></div>
					</div>
					<div class="flex-row">
						<div class="flex-left">Vận chuyển</div>
						<div class="flex-right"><strong class="c-red" name="shipping">{!! !empty($price_shipping) ? number_format($price_shipping, 0,',','.') . '</strong><i class="sb sb-vnd"></i>' : '0</strong><i class="sb sb-vnd"></i>' !!}</div>
					</div>

					<hr>
					
					<div class="flex-row total-bill">
						<div class="flex-left">
							<strong>TỔNG CỘNG:</strong>
						</div>
						<div class="flex-right"><strong class="c-red fz-up-3" id="total">{{ !empty($price_shipping) ? number_format(Cart::subtotal(0,',','') + $price_shipping, 0,',','.') : Cart::subtotal(0,',','.') }}</strong><i class="sb sb-vnd"></i></div>
					</div>

					<div class="form-group">
						<button class="btn default__btn-checkout btn-block btn-lg py-4" >ĐẶT MUA</button>
					</div>

					<div class="form-group">
						<label>Ghi chú đơn hàng</label>
						<textarea name="note" rows="3" class="form-control" ></textarea>
					</div>
				</div>
			</aside>
		</div>
	</div>
</form>
</div>
@stop