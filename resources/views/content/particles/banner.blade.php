<div class="container mt-5 mt-lg-3">
	<div class="row">
		<div class="col-12 col-xl-3 order-1 order-sm-0">
			<div class="home-category">
				<div class="block-title d-none d-lg-flex">
					<div class="bar"><i class="fa fa-bars" aria-hidden="true"></i></div>
					<span>DANH MỤC SẢN PHẨM</span>
				</div>
				<div class="block-content">
					<ul>
						@foreach($categories as $category)
						@if($category->id != 0)
						<li>
							<a href="products.html"  class="align-middle">
								<span class="icon-wrap"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
								<span class="content">{{ $category->name }}</span>
							</a>
						</li>
						@endif
						@endforeach
					</ul>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-xl-9 pl-xl-0 order-0 order-lg-1">
			<div class="home-banner">
				<div class="block-top d-none d-xl-flex">
					<div class="item ml-5">
						<a href="" class="delivery-2h">
							<i class="icon-2h"></i>
							Nhận hàng trong 2 tiếng <br>hàng chục nghìn sản phẩm
						</a>
					</div>
					<div class="item ml-5">
						<a href="" class="real-100">
							<i class="icon-100"></i>
							Tất cả sản phẩm <br>100% chính hãng
						</a>
					</div>
					<div class="item ml-5">
						<a href="" class="support">
							<i class="icon-support"></i>
							Hỗ trợ khách hàng <br>1900 6035 (1.000đ/phút)
						</a>
					</div>
				</div>
				<div class="block-slider">
					<div class="owl-carousel" id="owlSlider">
						@for($i = 1 ; $i <= 4 ; $i++)
						<div class="item">
							<img class="d-block w-100" src="{!! asset('images/content/slider/' . 1 . '.jpg') !!}" alt="First slide">
						</div>
						@endfor
					</div>
				</div>
			</div>
		</div>
	</div>
</div>