<div class="container">
	<div class="block-avertise">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-6 pr-lg-1">
				<div class="avertise-img">
					<a href="products.html"><img src="{!! asset('images/content/flash-sale/1.jpg') !!}" alt=""></a>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-lg-6 pl-lg-1 mt-2 mt-lg-0">
				<div class="avertise-img">
					<a href="products.html"><img src="{!! asset('images/content/flash-sale/2.jpg') !!}" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</div>