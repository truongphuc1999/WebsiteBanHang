@extends('content.layouts.master')

@section('content')
	<div class="py-5 text-center">
		<h3>Đặt hàng thành công với đơn hàng</h3>
		<h2 style="color:grey">#{{ $bill->id }}</h2>
		<a href="{{route('home')}}"><button>Tiếp tục mua hàng</button></a>
	</div>
@stop