<div class="product-item">
	<div class="product-img">
		<a href="{{ url('san-pham/' . $product->slug) }}"><img src="{!! asset('images/content/products/'.$product->image) !!}" alt=""></a>
	</div>
	@if($product->percent_sale != 0)
		<div class="percent-sale">
			- {{$product->percent_sale}}%
		</div>
	@endif
	<div class="product-name">
		<a href="{{ url('san-pham/' . $product->slug) }}">{{$product->name}}</a>
	</div>
	<div class="product-price">
		<span class="price-sale">
			{{  number_format($product->price - ($product->price / 100 * $product->percent_sale),0,",",".") }} <u>đ</u>
		</span>
		@if($product->percent_sale != 0)
			<span class="price-old">
				{{ number_format($product->price,0,",",".") }} <u>đ</u>
			</span>
		@endif
	</div>
	<div class="product-rate">
		<i class="fa fa-star" aria-hidden="true"></i>
		<i class="fa fa-star" aria-hidden="true"></i>
		<i class="fa fa-star" aria-hidden="true"></i>
		<i class="fa fa-star" aria-hidden="true"></i>
		<i class="fa fa-star-half-o" aria-hidden="true"></i>
	</div>
</div>