<!-- START BLOCK PRIMARY 1 -->
<div class="block-primary">
	<div class="container">
		<div class="block-header">

			<ul class="nav nav-tabs justify-content-between" id="myTab1" role="tablist">
				<li class="nav-title">
					<span><strong>Thời trang, Giày dép & Trang sức</strong></span>
				</li>
			</ul>
		</div>
		<div class="block-body">
			<div class="block-products">
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="danOng" role="tabpanel" aria-labelledby="home-tab">
						<div class="owl-carousel owl-products">
							@foreach($products as $product)
								@include('content.particles.product-item')
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
			<!-- END BLOCK PRIMARY 1 -->