@extends('content.layouts.master')

@section('content')
<div class="page-info-product">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-9">
				<form action="{{ Route('addCart') }}" method="POST">
				{!!csrf_field()!!}
				<div class="block-info-product">
					<div class="row">
						<div class="col-12 col-sm-12 col-lg-7">
							<div class="xzoom-container">
								<img class="xzoom" xoriginal="{!! asset('images/content/products/'.$product->image) !!} id="main_image" width="100%" src="{!! asset('images/content/products/'.$product->image) !!}" style="width: 100%;">

								<div class="xzoom-thumbs scrollbar-hidden">
									<!-- Thumbnails -->
									@foreach($images_detail as $image)
										<a href="{{ asset('images/content/products/' . $image->name) }}">
											<img class="xzoom-gallery" width="60" src="{{ asset('images/content/products/' . $image->name) }}">
										</a>
									@endforeach
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-lg-5 mt-3 mt-lg-0">
							<div class="name-product">
								<h4><strong>{{ $product->name }}</strong></h4>
								<div class="status">
									<span>
										Mã sản phẩm: 
										<strong>#000{{ $product->id }}</strong>
										<input type="hidden" name="product_id" value="{{ $product->id }}">
									</span>
									<span>
										Tình trạng:
										<strong>{{ $product->quantity > 0 ? "Còn hàng" : "Hết hàng" }}</strong>
									</span>
								</div>
							</div><hr>
							<div class="price-product">
								<span>Giá hiện tại: </span><strong  class="price-now">{{  number_format($product->price - ($product->price / 100 * $product->percent_sale),0,",",".") }} <u>đ</u></strong><br>
								<span class="price-default"><strike>Giá gốc: {{ number_format($product->price,0,",",".") }}  <u>đ</u></strike></span>
							</div><hr>
							<!-- <form> -->
								<div class="form-group">
									<div class="form-inline justify-content-between">
										<div class="custom-number">
											<div class="input-group">
												<span class="input-group-btn">
													<button type="button" class="btn" onclick="quantity_sub(event)">-</button>
												</span>
												<input type="text" value="1" min="1" max="100" id="quantity" name="quantity">
												<span class="input-group-btn">
													<button type="button" class="btn"  onclick="quantity_add(event)">+</button>
												</span>
											</div>
										</div>
											<div class="add-to-cart">
												<button class="btn btn-add-to-cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> THÊM VÀO GIỎ HÀNG</button>
											</div>
											@if(Session::has('message'))
											<div class="arlert arlert-success">{{Session::get('message')}}</div>
											@endif
								</div>
								<!-- </form> -->
							</div>
						</div>
					</div>
					<div class="block-content-product py-2 py-lg-4">
						<div class="block-header">
							<div class="block-title">
								<strong>THÔNG TIN SẢN PHẨM</strong>
							</div>
							<div class="block-content">
								{!!$product->description!!}
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
@endsection