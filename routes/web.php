<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/',function(){
	return redirect()->route('home');
});

// * Home *
Route::get('home','Content\HomeController@index')->name('home');

// * Login *
Route::get('login','Auth\LoginController@getLogin')->name('login');
Route::post('login','Auth\LoginController@postLogin')->name('postLogin');

Route::get('login/{social}','Auth\SocialAuthController@redirectToProvider')->name('loginSocial');
Route::get('login/{social}/callback','Auth\SocialAuthController@handleProviderCallback');

// * Logout *
Route::get('logout','Auth\LoginController@logout')->name('logout');

// * Register *
Route::get('register','Auth\RegisterController@getRegister')->name('register');
Route::post('register','Auth\RegisterController@postRegister')->name('postRegister');

// ---------------------- ĐÂY LÀ KHU VỰC ADMIN ----------------------

Route::group(['prefix' => 'admin', 'middleware' => 'admin'],function(){
	Route::get('/',function(){
		return view('admin.dashboard');
	});

	// Category
	Route::resource('categories','Admin\CategoryController');
		//Cập nhật danh mục sản phẩm
		Route::post('categories/{id}','Admin\CategoryController@update')->name('postCategories.update');

	// Product
	Route::resource('products','Admin\ProductController');
		//Cập nhật sản phẩm
		Route::post('products/{id}','Admin\ProductController@update')->name('postProducts.update');

	//Order
	Route::resource('bills','Admin\BillController');

	//Transport
	Route::resource('tranports','Admin\TranportController');
});

//--------------------------------------------------------------------------------------------------------------

//  ------------------- ĐÂY LÀ KHU VỰC CONTENT  -------------------

// * Sản phẩm *
Route::group(['prefix' => 'san-pham'],function(){
	// Chi tiết sản phẩm
	Route::get('{product_slug}','Content\ProductController@showProductDetail')->name('productDetail');
});

// * Giỏ hàng *
Route::group(['prefix' => 'gio-hang'], function(){
	//Hiển thị giỏ hàng
	Route::get('/','Content\CartController@index')->name('cart');

	//Thêm sản phẩm vào giỏ hàng
	Route::post('add','Content\CartController@add')->name('addCart');

	//Xóa sản phẩm trong giỏ hàng
	Route::get('delete','Content\CartController@delete')->name('deleteCart');

	//Xóa *toàn bộ* sản phẩm trong giỏ hàng
	Route::get('destroy','Content\CartController@destroy')->name('destroyCart');

	//Cập nhật sản phẩm trong giỏ hàng
	Route::post('update','Content\CartController@update')->name('updateCart');
});

// * Đặt hàng *
Route::group(['prefix' => 'dat-hang'], function(){
	Route::get('/', 'Content\OrderController@index')->name('getOrder');
	Route::post('/','Content\OrderController@store')->name('postOrder');

	Route::get('load-districts','Content\AreaAjaxController@loadDistricts');
	Route::get('load-wards','Content\AreaAjaxController@loadWards');
	Route::get('load-cul-shipping','Content\OrderController@loadPriceShipping');
});